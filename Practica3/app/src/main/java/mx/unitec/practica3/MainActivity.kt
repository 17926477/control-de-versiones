package mx.unitec.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Toast.makeText(this,
            position.toString() + ": "+ parent?.getItemAtPosition(position).toString(),
            Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val spinner: Spinner = findViewById(R.id.spinnerAlcaldia)
        spinner.onItemSelectedListener = this
        // Crea un ArrayAdapter utilizando el arreglo de alcaldías y el diseño default
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.simple_spinner_item
        )
        // Especifica el diseño a utilizar cuando la lista de opciones se muestra
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Aplica el adaptador al control Spinner
        spinner.adapter = adapter
    }
}
