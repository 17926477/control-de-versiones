package mx.unitec.practica3

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.view.View
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_picker.*
import mx.unitec.practica3.ui.DatePickerFragment
import mx.unitec.practica3.ui.TimePickerFragment

class PickerActivity : AppCompatActivity() {

    private lateinit var pkrTime: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)

        pkrTime = findViewById(R.id.pkrTime)
    }

    fun showTimePickerDialog(v: View) {
        val newFragment = TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener
        { _, hourOfDay, minute ->

            pkrTime.setText("${hourOfDay}:${minute}")

//            val simple = "%02d:%02d"
//            pkrTime.setText(simple.format(hourOfDay, minute))
        })
        newFragment.show(supportFragmentManager, "timePicker")
    }

    fun showDatePickerDialog(v: View) {
        val newFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener
        { _, year, month, dayOfMonth ->
            pkrDate.setText("$dayOfMonth/$month/$year")
        })
        newFragment.show(supportFragmentManager, "datePicker")
    }
}